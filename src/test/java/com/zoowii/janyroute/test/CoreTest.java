package com.zoowii.janyroute.test;

import com.zoowii.janyroute.core.AnyRoute;
import com.zoowii.janyroute.core.Route;
import com.zoowii.janyroute.core.RouteParamBinding;
import com.zoowii.janyroute.core.RouteTable;
import com.zoowii.janyroute.http.HttpAnyRoute;
import com.zoowii.janyroute.http.HttpRouteTable;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by zoowii on 2015/1/8.
 */
public class CoreTest {
    private static final Logger LOG = Logger.getLogger(CoreTest.class);
    private String url = "/user/123/view/any-note/abc/def";
    private String route = "/user/:id/view/:project/:*path";

    @Test
    public void testCoreRoute() {
        List<RouteParamBinding> bindings = AnyRoute.makeRoute(route).apply(url);
        Assert.assertEquals(bindings.size(), 3);
    }

    @Test
    public void testCoreUnRoute() {
        String unRouted = AnyRoute.makeUnRoute(route, 123, "dd", "ddfa/dd");
        Assert.assertEquals(unRouted, "/user/123/view/dd/ddfa/dd");
    }

    @Test
    public void testCoreRouteTable() {
        RouteTable routeTable = AnyRoute.makeRouteTable(
                AnyRoute.route("/test/:id/update", "test_handler", "test"),
                AnyRoute.contextRoute("/user",
                        AnyRoute.route("/:id/view/:project/:*path", "view_user_handler", "view_user"))
        );
        Route viewUserRoute = routeTable.findRoute("view_user");
        Assert.assertEquals("view_user_handler", viewUserRoute.getHandler());
        Route matched = routeTable.match(url);
        Assert.assertEquals(matched.getBindings().size(), 3);
        String reversed = routeTable.urlFor("view_user", "433", "test-project", "github.com/zoowii");
        LOG.info("core reversed url is " + reversed);
        Assert.assertEquals("/user/433/view/test-project/github.com/zoowii", reversed);
    }

    @Test
    public void testHttpRoute() {
        Route[] userRoutes = {
                HttpAnyRoute.GET("/:id/view/:project/:*path", "view_user_handler", "view_user"),
                HttpAnyRoute.POST("/:id/view/:project/:*path", "update_user_handler", "update_user")
        };
        HttpRouteTable routeTable = HttpAnyRoute.makeRouteTable(
                HttpAnyRoute.GET("/test/:id/update", "test_handler", "test"),
                AnyRoute.contextRoute("/user", userRoutes)
        );
        Route updateUserRoute = routeTable.findRoute("update_user");
        Route matched = routeTable.httpMatch(HttpAnyRoute.GET_METHOD, url);
        String reversed = routeTable.urlFor("update_user", "433", "test-project", "github.com/zoowii");
        LOG.info("http reversed url is " + reversed);
        Assert.assertEquals(updateUserRoute.getHandler(), "update_user_handler");
        Assert.assertEquals(matched.getBindings().get(2).getValue(), "abc/def");
        Assert.assertEquals(matched.getBindings().get(0).getValue(), "123");
        Assert.assertEquals("/user/433/view/test-project/github.com/zoowii", reversed);
    }
}
