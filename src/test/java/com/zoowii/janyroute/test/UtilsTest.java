package com.zoowii.janyroute.test;

import com.zoowii.janyroute.core.AnyRoute;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by zoowii on 2015/1/7.
 */
public class UtilsTest {
    @Test
    public void testGetRouteParams() {
        String url = "/abc/:name/:age/test/:*path";
        Assert.assertEquals(3, AnyRoute.getRouteParams(url).size());
    }
}
