package com.zoowii.janyroute.core;

import com.google.common.base.Function;
import com.zoowii.janyroute.util.Function2;
import com.zoowii.janyroute.util.ListUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by zoowii on 2015/1/8.
 */
public class RouteTable implements IRouteTable {
    private List<Route> routes = new ArrayList<Route>();

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public void addRoute(Route route) {
        routes.add(route);
    }

    public void addRoutes(Route... routes) {
        this.routes.addAll(Arrays.asList(routes));
    }

    public void addRoutes(Collection<Route> routes) {
        this.routes.addAll(routes);
    }

    public RouteTable() {
    }

    public RouteTable(List<Route> routes) {
        this.routes = routes;
    }

    @Override
    public Route findRoute(final String name) {
        return coreFindRoute(name);
    }

    /**
     * 在路由表中根据名字找到路由, 如果路由在context中,则补全完整的路由
     *
     * @param name
     * @return
     */
    public Route coreFindRoute(final String name) {
        return ListUtil.firstNotEmpty(ListUtil.map(this.getRoutes(), new Function<Route, Route>() {
            @Override
            public Route apply(Route route) {
                if (route.isContextRoute()) {
                    ContextRoute contextRoute = (ContextRoute) route;
                    return new RouteTable(contextRoute.getRoutes()).findRoute(name);
                } else {
                    if (route.getName().equals(name)) {
                        return route;
                    } else {
                        return null;
                    }
                }
            }
        }));
    }

    @Override
    public String reverseUsingFn(String name, Function2<RouteTable, String, Route> findFn, Object... params) {
        return coreReverseUsingFn(name, findFn, params);
    }

    public String coreReverseUsingFn(String name, Function2<RouteTable, String, Route> findFn, Object... params) {
        Route route = findFn.apply(this, name);
        if (route == null) {
            return null;
        }
        return AnyRoute.makeUnRoute(route.getRoute().toString(), params);
    }

    @Override
    public String reverse(String name, Object... params) {
        return coreReverse(name, params);
    }

    public String coreReverse(String name, Object... params) {
        return coreReverseUsingFn(name, new Function2<RouteTable, String, Route>() {
            @Override
            public Route apply(RouteTable param1, String param2) {
                return param1.coreFindRoute(param2);
            }
        }, params);
    }

    @Override
    public String urlFor(String name, Object... params) {
        return coreUrlFor(name, params);
    }

    public String coreUrlFor(String name, Object... params) {
        return coreReverse(name, params);
    }

    @Override
    public Route match(String source) {
        return coreMatch(source);
    }

    /**
     * 在一个路由表中找到映射
     *
     * @param source
     * @return
     */
    public Route coreMatch(final String source) {
        return ListUtil.firstNotEmpty(ListUtil.map(getRoutes(), new Function<Route, Route>() {
            @Override
            public Route apply(Route route) {
                if (route.isContextRoute()) {
                    ContextRoute contextRoute = (ContextRoute) route;
                    return new RouteTable(contextRoute.getRoutes()).coreMatch(source);
                } else {
                    RouteFunction routeFn = route.getRouteFn();
                    if (routeFn == null) {
                        return null;
                    }
                    List<RouteParamBinding> bindings = routeFn.apply(source);
                    if (bindings == null) {
                        return null;
                    }
                    route = (Route) route.clone();
                    route.setBindings(bindings);
                    route.setSource(source);
                    return route;
                }
            }
        }));
    }
}
