package com.zoowii.janyroute.core;

import org.apache.commons.beanutils.BeanUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zoowii on 2015/1/8.
 */
public class ContextRoute extends Route {
    private String context;
    private List<Route> routes = new ArrayList<Route>();

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public ContextRoute(String context, Route... routes) {
        this(context, Arrays.asList(routes));
    }

    public ContextRoute(String context, List<Route> routes) {
        this.context = context;
        if (routes != null) {
            this.routes.addAll(routes);
        }
    }

    public ContextRoute() {

    }
}
