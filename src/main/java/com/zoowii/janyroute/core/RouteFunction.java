package com.zoowii.janyroute.core;

import com.google.common.base.Function;

import java.util.List;

/**
 * Created by zoowii on 2015/1/8.
 */
public interface RouteFunction extends Function<String, List<RouteParamBinding>> {
}
