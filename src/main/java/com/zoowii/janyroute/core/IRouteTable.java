package com.zoowii.janyroute.core;

import com.zoowii.janyroute.util.Function2;

/**
 * Created by zoowii on 15/1/10.
 */
public interface IRouteTable {
    /**
     * 在一个路由表中找到映射
     *
     * @param source
     * @return
     */
    public Route match(String source);

    /**
     * 路由反转
     *
     * @param name
     * @param params
     * @return
     */
    public String urlFor(String name, Object... params);

    /**
     * 路由反转
     *
     * @param name
     * @param params
     * @return
     */
    public String reverse(String name, Object... params);

    /**
     * 使用某个函数来进行路由反转
     *
     * @param name
     * @param findFn
     * @param params
     * @return
     */
    public String reverseUsingFn(String name, Function2<RouteTable, String, Route> findFn, Object... params);

    /**
     * 在路由表中根据名字找到路由, 如果路由在context中,则补全完整的路由
     *
     * @param name
     * @return
     */
    public Route findRoute(final String name);
}
