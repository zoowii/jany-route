package com.zoowii.janyroute.core;

/**
 * Created by zoowii on 2015/1/7.
 */
public class RouteParamBinding {
    private String name;
    private String value;

    public RouteParamBinding(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public RouteParamBinding() {
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        String shortName = name;
        if (shortName.startsWith(":*")) {
            shortName = shortName.substring(2);
        }
        if (shortName.startsWith(":")) {
            shortName = shortName.substring(1);
        }
        return shortName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static RouteParamBinding of(String name, String value) {
        return new RouteParamBinding(name, value);
    }
}
