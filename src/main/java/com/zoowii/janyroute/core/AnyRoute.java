package com.zoowii.janyroute.core;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.zoowii.janyroute.util.AnyRouteUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by zoowii on 15/1/10.
 */
public class AnyRoute {
    private static final String defaultSep = "/";
    private static final Pattern routeParamPattern = Pattern.compile("((?!\\\\):(\\\\\\*)?[a-zA-Z_][a-zA-Z_0-9]*)");

    private static final LoadingCache<String, List<String>> routeParamsCache = CacheBuilder.newBuilder()
            .expireAfterAccess(10, TimeUnit.MINUTES)
            .build(new CacheLoader<String, List<String>>() {
                @Override
                public List<String> load(String route) throws Exception {
                    return realFetchRouteParams(route);
                }
            });

    private static List<String> realFetchRouteParams(String route) {
        route = AnyRouteUtils.reEscape(route);
        return AnyRouteUtils.reMatches(routeParamPattern, route);
    }

    /**
     * 创建一个只有在route规则匹配时才会返回非false/nil的函数(实际返回的应该是dest和params)
     *
     * @param route
     * @return
     */
    public static List<String> getRouteParams(String route) {
        try {
            return routeParamsCache.get(route);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建一个只有在route规则匹配时才会返回非假的函数(实际返回的应该是dest和params)
     *
     * @param route
     * @return
     */
    public static RouteFunction makeRoute(String route) {
        final List<String> routeParams = getRouteParams(route);
        final String sourceRoute = route;
        route = AnyRouteUtils.reEscape(route);
        route = route.replaceAll("(:[a-zA-Z_][a-zA-Z_0-9]*)", "([^" + defaultSep + "]+)");
        route = route.replaceAll("(:\\\\\\*[a-zA-Z_][a-zA-Z_0-9]*)", "(.+)");
        route = "^" + route + "$";
        final Pattern routePattern = Pattern.compile(route);
        return new RouteFunction() {
            @Override
            public List<RouteParamBinding> apply(String source) {
                Matcher matcher = routePattern.matcher(source);
                List<String> matches = new ArrayList<>();
                if (matcher.find()) {
                    for (int i = 0; i < matcher.groupCount(); ++i) {
                        matches.add(matcher.group(i + 1));
                    }
                }
                List<RouteParamBinding> result = new ArrayList<>();
                if (matches.size() > 0) {
                    for (int i = 0; i < routeParams.size(); ++i) {
                        if (i < matches.size()) {
                            result.add(RouteParamBinding.of(routeParams.get(i), matches.get(i)));
                        }
                    }
                    return result;
                } else {
                    if (routeParams.size() < 1 && sourceRoute.equals(source)) {
                        return result;
                    } else {
                        return null;
                    }
                }
            }
        };
    }

    /**
     * 反转路由的函数
     *
     * @param route
     * @param params
     * @return
     */
    public static String makeUnRoute(String route, Object... params) {
        List<String> routeParams = getRouteParams(route);
        List<RouteParamBinding> routeParamBindings = new ArrayList<>();
        for (int i = 0; i < routeParams.size(); ++i) {
            if (i < params.length) {
                routeParamBindings.add(RouteParamBinding.of(routeParams.get(i), params[i] != null ? params[i].toString() : ""));
            } else {
                routeParamBindings.add(RouteParamBinding.of(routeParams.get(i), ""));
            }
        }
        String url = route;
        for (RouteParamBinding routeParamBinding : routeParamBindings) {
            url = url.replaceAll("(?!\\\\):(\\*)?" + routeParamBinding.getShortName(), routeParamBinding.getValue());
        }
        return url;
    }

    public static Route route(Object routeStr, Object handler, String routeName, Object extraInfo) {
        return new Route(routeName, routeStr, extraInfo, handler);
    }

    public static Route route(Object routeStr, Object handler, String routeName) {
        return route(routeStr, handler, routeName, null);
    }

    public static Route route(Object routeStr, Object handler) {
        return route(routeStr, handler, "__route__" + UUID.randomUUID().toString());
    }

    public static ContextRoute contextRoute(String context, Route... routes) {
        return new ContextRoute(context, routes);
    }

    public static ContextRoute contextRoute(String context, List<Route> routes) {
        return new ContextRoute(context, routes);
    }

    public static RouteTable makeRouteTable(String context, Route... routes) {
        return makeRouteTable(context, Arrays.asList(routes));
    }

    public static RouteTable makeRouteTable(String context, List<Route> routes) {
        RouteTable routeTable = new RouteTable();
        if (routes != null) {
            for (Route route : routes) {
                if (route.isContextRoute()) {
                    ContextRoute contextRoute = (ContextRoute) route.clone();
                    contextRoute.setRoutes(makeRouteTable(context + contextRoute.getContext(), contextRoute.getRoutes()).getRoutes());
                    routeTable.addRoute(contextRoute);
                } else {
                    String routeStr = (route.getExtraInfo() != null ? route.getExtraInfo() : "") + context + route.getRoute();
                    route = (Route) route.clone();
                    route.setRoute(routeStr);
                    route.setRouteFn(makeRoute(routeStr));
                    route.setRouteParams(getRouteParams(routeStr));
                    routeTable.addRoute(route);
                }
            }
        }
        return routeTable;
    }

    public static RouteTable makeRouteTable(List<Route> routes) {
        return makeRouteTable("", routes);
    }

    public static RouteTable makeRouteTable(Route... routes) {
        return makeRouteTable(Arrays.asList(routes));
    }
}
