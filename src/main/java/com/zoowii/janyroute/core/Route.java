package com.zoowii.janyroute.core;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.exception.CloneFailedException;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by zoowii on 2015/1/8.
 */
public class Route {
    private String name;
    private Object route;
    private Object extraInfo;
    private Object handler;
    private RouteFunction routeFn;
    private List<String> routeParams;
    private List<RouteParamBinding> bindings;
    private String source;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getRoute() {
        return route;
    }

    public void setRoute(Object route) {
        this.route = route;
    }

    public Object getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(Object extraInfo) {
        this.extraInfo = extraInfo;
    }

    public Object getHandler() {
        return handler;
    }

    public void setHandler(Object handler) {
        this.handler = handler;
    }

    public RouteFunction getRouteFn() {
        return routeFn;
    }

    public void setRouteFn(RouteFunction routeFn) {
        this.routeFn = routeFn;
    }

    public List<String> getRouteParams() {
        return routeParams;
    }

    public void setRouteParams(List<String> routeParams) {
        this.routeParams = routeParams;
    }

    public List<RouteParamBinding> getBindings() {
        return bindings;
    }

    public void setBindings(List<RouteParamBinding> bindings) {
        this.bindings = bindings;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Route() {
    }

    public Route(String name, Object route, Object extraInfo, Object handler) {
        this.name = name;
        this.route = route;
        this.extraInfo = extraInfo;
        this.handler = handler;
    }

    public Object clone() {
        try {
            return BeanUtils.cloneBean(this);
        } catch (IllegalAccessException e) {
            throw new CloneFailedException(e);
        } catch (InstantiationException e) {
            throw new CloneFailedException(e);
        } catch (InvocationTargetException e) {
            throw new CloneFailedException(e);
        } catch (NoSuchMethodException e) {
            throw new CloneFailedException(e);
        }
    }

    public boolean isContextRoute() {
        return this instanceof ContextRoute;
    }
}
