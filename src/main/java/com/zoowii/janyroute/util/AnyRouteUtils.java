package com.zoowii.janyroute.util;

import com.google.common.base.Function;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by zoowii on 2015/1/7.
 */
public class AnyRouteUtils {
    private static final Set<Character> reChars;


    static {
        reChars = new HashSet<>();
        for (char c : "\\.*+|?()[]{}$^".toCharArray()) {
            reChars.add(c);
        }
    }

    public static String escape(String str, Function<Character, String> fn) {
        if (str == null || fn == null) {
            return str;
        }
        StringBuilder builder = new StringBuilder();
        for (char c : str.toCharArray()) {
            String escapedChars = fn.apply(c);
            if (escapedChars == null) {
                builder.append(c);
            } else {
                builder.append(escapedChars);
            }
        }
        return builder.toString();
    }

    /**
     * Escape all special regex chars in a string
     *
     * @param str
     * @return
     */
    public static String reEscape(String str) {
        return escape(str, new Function<Character, String>() {
            @Override
            public String apply(Character c) {
                return reChars.contains(c) ? String.format("\\%s", "" + c) : null;
            }
        });
    }

    public static List<String> reMatches(Pattern pattern, String source) {
        if (pattern == null || source == null) {
            return null;
        }
        List<String> matches = new ArrayList<>();
        Matcher matcher = pattern.matcher(source);
        while (matcher.find()) {
            matches.add(matcher.group());
        }
        return matches;
    }
}
