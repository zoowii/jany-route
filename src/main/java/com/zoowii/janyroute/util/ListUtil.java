package com.zoowii.janyroute.util;

import com.google.common.base.Function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by zoowii on 2015/1/8.
 */
public class ListUtil {

    public static <T> T firstNotEmpty(Collection<T> col) {
        if (col == null || col.size() < 1) {
            return null;
        }
        for (T item : col) {
            if (item != null && !item.equals(Boolean.FALSE)) {
                return item;
            }
        }
        return null;
    }

    public static <T1, T2> List<T2> map(T1[] source, Function<T1, T2> fn) {
        if (source == null || fn == null) {
            return null;
        }
        return map(Arrays.asList(source), fn);
    }

    public static <T1, T2> List<T2> map(List<T1> source, Function<T1, T2> fn) {
        if (source == null || fn == null) {
            return null;
        }
        List<T2> result = new ArrayList<>();
        for (T1 item : source) {
            result.add(fn.apply(item));
        }
        return result;
    }
}
