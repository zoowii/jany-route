package com.zoowii.janyroute.util;

/**
 * Created by zoowii on 2015/1/7.
 */
public interface Action<T> {
    public void apply(T param);
}
