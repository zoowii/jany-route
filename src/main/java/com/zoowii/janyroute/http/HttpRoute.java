package com.zoowii.janyroute.http;

import com.zoowii.janyroute.core.Route;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.exception.CloneFailedException;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by zoowii on 15/1/10.
 */
public class HttpRoute extends Route {
    private String method;


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public HttpRoute() {

    }

    public HttpRoute(Route route, String method) {
        try {
            BeanUtils.copyProperties(this, route);
            this.setName(HttpAnyRoute.rmHttpMethodPrefix(route.getName()));
            this.setRoute(HttpAnyRoute.rmHttpMethodPrefix((String) route.getRoute()));
            this.setMethod(method);
        } catch (IllegalAccessException e) {
            throw new CloneFailedException(e);
        } catch (InvocationTargetException e) {
            throw new CloneFailedException(e);
        }
    }
}
