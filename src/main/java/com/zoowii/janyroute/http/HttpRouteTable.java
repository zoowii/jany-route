package com.zoowii.janyroute.http;

import com.google.common.base.Function;
import com.zoowii.janyroute.core.Route;
import com.zoowii.janyroute.core.RouteTable;
import com.zoowii.janyroute.util.Function2;
import com.zoowii.janyroute.util.ListUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.exception.CloneFailedException;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by zoowii on 15/1/10.
 */
public class HttpRouteTable extends RouteTable {
    public HttpRouteTable() {

    }

    public HttpRouteTable(RouteTable routeTable) {
        try {
            BeanUtils.copyProperties(this, routeTable);
        } catch (IllegalAccessException e) {
            throw new CloneFailedException(e);
        } catch (InvocationTargetException e) {
            throw new CloneFailedException(e);
        }
    }

    @Override
    public Route match(String source) {
        throw new UnsupportedOperationException();
    }

    public Route httpMatch(String method, String source) {
        Route findResult = coreMatch(HttpAnyRoute.parseHttpMethodToString(method) + "_" + source);
        if (findResult == null) {
            return null;
        }
        HttpRoute result = new HttpRoute(findResult, method);
//        result.setSource(source);
        return result;
    }

    @Override
    public String urlFor(String name, Object... params) {
        return httpUrlFor(name, params);
    }

    public String httpUrlFor(String name, Object... params) {
        return httpReverse(name, params);
    }

    @Override
    public String reverse(String name, Object... params) {
        return httpReverse(name, params);
    }

    public String httpReverse(String name, Object... params) {
        String reverseResult = coreReverseUsingFn(name, HttpAnyRoute.findRouteInHttpRouteTableFn, params);
        if (reverseResult == null) {
            return null;
        }
        return HttpAnyRoute.rmHttpMethodPrefix(reverseResult);
    }

    @Override
    public String reverseUsingFn(String name, Function2<RouteTable, String, Route> findFn, Object... params) {
        return coreReverseUsingFn(name, findFn, params);
    }

    @Override
    public Route findRoute(String name) {
        return httpFindRoute(name);
    }

    public Route httpFindRoute(final String name) {
        return ListUtil.firstNotEmpty(ListUtil.map(HttpAnyRoute.httpMethods, new Function<String, Route>() {
            @Override
            public Route apply(String method) {
                String mName = HttpAnyRoute.parseHttpMethodToString(method) + "_" + name;
                return coreFindRoute(mName);
            }
        }));
    }
}
