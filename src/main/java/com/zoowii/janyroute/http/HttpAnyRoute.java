package com.zoowii.janyroute.http;

import com.google.common.base.Function;
import com.zoowii.janyroute.core.AnyRoute;
import com.zoowii.janyroute.core.Route;
import com.zoowii.janyroute.core.RouteTable;
import com.zoowii.janyroute.util.Function2;
import com.zoowii.janyroute.util.ListUtil;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by zoowii on 15/1/10.
 */
public class HttpAnyRoute {
    public static final String GET_METHOD = "get";
    public static final String POST_METHOD = "post";
    public static final String PUT_METHOD = "put";
    public static final String DELETE_METHOD = "delete";
    public static final String HEAD_METHOD = "head";
    public static final String OPTION_METHOD = "option";
    public static final String ANY_METHOD = "any";
    public static final String[] httpMethods = {GET_METHOD, POST_METHOD, PUT_METHOD, DELETE_METHOD, HEAD_METHOD, OPTION_METHOD};

    public static String parseHttpMethodToString(String method) {
        return method.toLowerCase();
    }

    public static String rmHttpMethodPrefix(String str) {
        int idx = str.indexOf("_");
        if (idx >= 0) {
            return str.substring(idx + 1);
        } else {
            return str;
        }
    }

    public static Route httpRoute(String method, Object routeStr, Object handler) {
        return httpRoute(method, routeStr, handler, "__route__" + UUID.randomUUID().toString());
    }

    /**
     * 建立http的route表. 如果method使用:ANY,则映射到所有已知http method
     *
     * @param method
     * @param routeStr
     * @param handler
     * @param routeName
     * @return
     */
    public static Route httpRoute(String method, final Object routeStr, final Object handler, final String routeName) {
        if (ANY_METHOD.equals(parseHttpMethodToString(method))) {
            return AnyRoute.contextRoute("", ListUtil.map(httpMethods, new Function<String, Route>() {
                @Override
                public Route apply(String method) {
                    return httpRoute(method, routeStr, handler, routeName);
                }
            }));
        } else {
            String rStr = parseHttpMethodToString(method) + "_" + routeName;
            return AnyRoute.route(routeStr, handler, rStr, parseHttpMethodToString(method) + "_");
        }
    }

    public static Function2<RouteTable, String, Route> findRouteInHttpRouteTableFn = new Function2<RouteTable, String, Route>() {
        @Override
        public Route apply(RouteTable param1, String param2) {
            return param1.findRoute(param2);
        }
    };

    public static Route quickHttpMethodRoute(String method, Object pattern, Object handler, String routeName) {
        return httpRoute(parseHttpMethodToString(method), pattern, handler, routeName);
    }

    public static Route quickHttpMethodRoute(String method, Object pattern, Object handler) {
        return httpRoute(parseHttpMethodToString(method), pattern, handler);
    }

    public static Route GET(Object pattern, Object handler, String routeName) {
        return quickHttpMethodRoute(GET_METHOD, pattern, handler, routeName);
    }

    public static Route POST(Object pattern, Object handler, String routeName) {
        return quickHttpMethodRoute(POST_METHOD, pattern, handler, routeName);
    }

    public static Route PUT(Object pattern, Object handler, String routeName) {
        return quickHttpMethodRoute(PUT_METHOD, pattern, handler, routeName);
    }

    public static Route DELETE(Object pattern, Object handler, String routeName) {
        return quickHttpMethodRoute(DELETE_METHOD, pattern, handler, routeName);
    }

    public static Route HEAD(Object pattern, Object handler, String routeName) {
        return quickHttpMethodRoute(HEAD_METHOD, pattern, handler, routeName);
    }

    public static Route OPTION(Object pattern, Object handler, String routeName) {
        return quickHttpMethodRoute(OPTION_METHOD, pattern, handler, routeName);
    }

    public static Route ANY(Object pattern, Object handler, String routeName) {
        return quickHttpMethodRoute(ANY_METHOD, pattern, handler, routeName);
    }

    public static Route GET(Object pattern, Object handler) {
        return quickHttpMethodRoute(GET_METHOD, pattern, handler);
    }

    public static Route POST(Object pattern, Object handler) {
        return quickHttpMethodRoute(POST_METHOD, pattern, handler);
    }

    public static Route PUT(Object pattern, Object handler) {
        return quickHttpMethodRoute(PUT_METHOD, pattern, handler);
    }

    public static Route DELETE(Object pattern, Object handler) {
        return quickHttpMethodRoute(DELETE_METHOD, pattern, handler);
    }

    public static Route HEAD(Object pattern, Object handler) {
        return quickHttpMethodRoute(HEAD_METHOD, pattern, handler);
    }

    public static Route OPTION(Object pattern, Object handler) {
        return quickHttpMethodRoute(OPTION_METHOD, pattern, handler);
    }

    public static Route ANY(Object pattern, Object handler) {
        return quickHttpMethodRoute(ANY_METHOD, pattern, handler);
    }

    public static HttpRouteTable makeRouteTable(String context, Route... routes) {
        return new HttpRouteTable(AnyRoute.makeRouteTable(context, routes));
    }

    public static HttpRouteTable makeRouteTable(String context, List<Route> routes) {
        return new HttpRouteTable(AnyRoute.makeRouteTable(context, routes));
    }

    public static HttpRouteTable makeRouteTable(List<Route> routes) {
        return new HttpRouteTable(AnyRoute.makeRouteTable(routes));
    }

    public static HttpRouteTable makeRouteTable(Route... routes) {
        return new HttpRouteTable(AnyRoute.makeRouteTable(routes));
    }
}
