jany-route
====
simple string match library, maybe useful in web framework

一个简单的双向路由库(路由 <=> handler + params的双射),主要可以用于Java,也可以独立使用

提供了基本的路由core和http封装

Link to: https://github.com/zoowii/any-route (Clojure), https://github.com/zoowii/any-route.js (JavaScript)

## License

Copyright © 2015 zoowii

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.